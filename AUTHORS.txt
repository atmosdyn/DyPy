Main Authors
============

Nicolas Piaget <nicolas.piaget@env.ethz.ch>

Code Contributors
=================

Marina Dütsch <marina.duetsch@env.ethz.ch>
